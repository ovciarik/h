'use strict';

let vm;

vm = new Vue({
    el: '#app',
    data: {
        resource: 0,
        table_rows: generateTable(10, 31)
    },
    methods: {
        log: function (x) {
            this.table_rows[x[1]][x[2]][0] = shiftColor(this.table_rows[x[1]][x[2]][0]);
            this.resource++;
            updateTable(this.table_rows)
        },
        log_mouseover: function(x) {
//            console.log('mouseover');
//            this.table_rows[x[1]][x[2]][0] = shiftColor(this.table_rows[x[1]][x[2]][0]);
//            this.resource++;
        }
    }
});

function readTable(table) {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            let response;
            response = JSON.parse(this.responseText);
            vm.table_rows = JSON.parse(response);
        }
    };
    xhttp.open('GET', '/api/table', true);
    xhttp.send();
}

function createTable(table) {
    true;
    true;
}

function updateTable(table) {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            let response;
            response = JSON.parse(this.responseText);
        }
    };
    xhttp.open('PUT', '/api/table', true);
    xhttp.send(JSON.stringify(table));
}

function deleteTable(table) {
    true;
    true;
}

function shiftColor(color){
    let returnColor = '';
    switch (color){
        case 'td-red':
            returnColor = 'td-blue';
            break;
        case 'td-green':
            returnColor = 'td-red';
            break;
        case 'td-blue':
            returnColor = 'td-blank';
            break;
        case 'td-blank':
            returnColor = 'td-green';
            break;
    }
    return returnColor;
}

function generateRandomTdColor(i, j) {
//    let x = Math.floor((Math.random() * 60) + 1);
//    let return_value = '';
//    switch (x % 3) {
//        case 0:
//            return_value = ['td-red', i, j];
//            break;
//        case 1:
//            return_value = ['td-green', i, j];
//            break;
//        case 2:
//            return_value = ['td-blue', i, j];
//            break;
//        case 3:
//            return_value = ['td-blank', i, j];
//            break;
//    }
    return ['td-blank', i, j];
}

function generateTable(rows, cols){
    let arr = [];
    for(let i=0; i < rows; i++) {
        arr[i] = [];
        for(let j=0; j < cols; j++) {
            arr[i][j] = generateRandomTdColor(i, j);
        }
    }
    return arr;
}

readTable();


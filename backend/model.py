from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Integer

import backend.database as database


class Resource(database.Base):
    __tablename__ = 'resource'
    id = Column(String(50), primary_key=True)

    def __init__(self, id):
        self.id = id

    def __repr__(self):
        return '<Resource(id={})>'.format(self.id)


class Table(database.Base):
    __tablename__ = 'table'
    id = Column(Integer(), primary_key=True)
    table = Column(String())

    def __init__(self, id, table):
        self.id = id
        self.table = table

    def __repr__(self):
        return '<Resource(table={})>'.format(self.table)

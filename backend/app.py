import json

import flask

import backend.database as database
import backend.model as model
from backend.database import db_session

app = flask.Flask(__name__)


def generate_table():
    return_table = []
    for x in range(10):
        return_table.append([])
        for y in range(31):
            return_table[x].append(['td-blank', x, y])

    return str(json.dumps(return_table))


@app.cli.command('initdb')
def command_initdb():
    print('Initiating database.')
    database.init_db()
    print('Database initialized.')


# table
@app.route('/api/table', methods=['GET'])
def get_table():
    try:
        with db_session() as session:
            response = session.query(model.Table).filter(model.Table.id == 0).first().table.replace("'", '"')
            return json.dumps(response)
    except:
        post_table()
        return get_table()


@app.route('/api/table', methods=['POST'])
def post_table():
    with db_session() as session:
        print(generate_table())
        tb = model.Table(0, generate_table())
        session.add(tb)
    response = []
    return json.dumps(response)


@app.route('/api/table', methods=['PUT'])
def put_table():
    with db_session() as session:
        tb = session.query(model.Table).filter(model.Table.id == 0).first()
        # tb = model.Table(0, (str(flask.request.get_json(force=True))))
        tb.table = str(flask.request.get_json(force=True))
        session.add(tb)

    response = []
    return json.dumps(response)


@app.route('/api/table', methods=['DELETE'])
def delete_table():
    response = []
    return json.dumps(response)


# static
@app.route('/<path:file_name>')
def static_fonts_file_server(file_name):
    print(file_name)
    return flask.send_from_directory('../frontend', file_name)


@app.route('/')
def hello():
    return flask.send_from_directory('../frontend', 'index.html')


if __name__ == "__main__":
    app.run()

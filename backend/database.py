from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from contextlib import contextmanager

engine = create_engine('sqlite:///../test.db', convert_unicode=True)
Base = declarative_base()


def init_db():
    import backend.model
    Base.metadata.create_all(bind=engine)


@contextmanager
def db_session():
    session = sessionmaker(bind=engine)()
    try:
        yield session
    finally:
        session.commit()
        session.close()
